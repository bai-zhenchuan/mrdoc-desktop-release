# mrdoc-desktop-release

当前版本：v0.2.4
发布日期：2022-12-13

## 介绍
MrDoc 觅思文档的桌面客户端，支持 Windows、Linux 和 macOS

借助 MrDoc桌面客户端，你可以在不打开浏览器的情况下对MrDoc的文集和文档进行编写、修改和其他处理。

## 安装教程

在 MrDoc桌面客户端的[下载页面](https://gitee.com/zmister/mrdoc-desktop-release/releases) 根据自己的操作系统，下载对应的安装包进行安装，其中：

- .exe格式的文件为 Windows 操作系统的安装包
- .deb格式的文件为 Linux 操作系统的安装包
- .dmg格式的文件为 macOS 操作系统的安装包

## 使用说明

MrDoc 桌面客户端需要添加MrDoc站点用户才能进行使用：

<img src="https://doc.mrdoc.pro/media/202204/%E6%88%AA%E5%B1%8F2022-04-04%20%E4%B8%8B%E5%8D%8812941_20220404133035071805.png" width="" />

### MrDoc地址

“MrDoc地址”表示你所使用的 MrDoc 的主机地址，比如：`https://doc.mrdoc.pro`，注意末尾不要带斜杠`/`。

### 用户token

“用户token”表示个人账号的用户token值，其可在网页端【个人中心】-【个人管理】-【Token管理】页面进行查看，如下图所示：

<img src="https://doc.mrdoc.pro/media/202204/%E6%88%AA%E5%B1%8F2022-04-04%20%E4%B8%8B%E5%8D%8813515_20220404133530496639.png" />

## 问题反馈和建议

你可以在本仓库的 issues 页面进行问题和建议的反馈，请务必提供完整的信息，包括但不限于：

- 操作系统类型和版本
- MrDoc版本
- MrDoc桌面客户端版本
- 问题操作路径
- 问题的截图或动图

## 产品动态

你可以关注 MrDoc官方微信公众号获取 MrDoc桌面客户端和其他MrDoc产品的动态：

<img src="https://doc.mrdoc.pro/media/202203/20220331121926_20220331122015390193.png" width="50%">